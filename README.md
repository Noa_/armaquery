#ArmAQuery

ArmA 2 and 3 Gamespy query class for C#

###Create objects

a3 = new ArmA3.ArmA3(ArmA3IP, ArmA3Port);

a2 = new ArmA2.ArmA2(ArmA2IP, ArmA2Port);

###Update

a3.Update();

a2.Update();

###Exposed details

| Type          | Name          |
| ------------- |:-------------:|
| string        | GameVersion   |
| string        | HostName      |
| string        | MapName       |
| string        | GameType      |
| int           | NumPlayers   |
| int           | NumTeam       |
| int           | MaxPlayers    |
| string        | GameMode    |
| string        | TimeLimit    |
| bool          | Password    |
| string        | CurrentVersion |
| string        | RequiredVersion |
| string        | Mod    |
| bool          | BattleEye    |
| double        | Longitude    |
| double        | Latitude    |
| List<string>  | Players   |
| string        | Mission    |
