﻿using System;
using System.Text;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ArmA2
{
    class ArmA2
    {
        public ServerInfo ServerInfo { get; protected set; }
        public PlayerCollection Players { get; protected set; }
        public string host;
        public int port;
        public ArmA2(string host, int port)
        {
            this.host = host;
            this.port = port;
        }

        public void Update()
        {
            try
            {
                QueryClient client = new QueryClient();
                PacketCollection packets = client.QueryServer(this.host, this.port);
                string dataString = packets.GetDataString();
                string[] data = Regex.Split(
                    dataString,
                    Encoding.ASCII.GetString(new byte[] { 00, 00, 01 }),
                    RegexOptions.Compiled);

                this.ServerInfo = ServerInfo.Parse(data[0]);
                this.Players = PlayerCollection.Parse(data[1]);
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Sock Error - " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Error - " + ex.Message);
            }
        }

    }

    class QueryClient
    {
        private readonly byte[] timestamp = GetTimeStamp();

        public static int GetTimeStampInt()
        {
            return BitConverter.ToInt32(GetTimeStamp(), 0);
        }

        public static byte[] GetTimeStamp()
        {
            DateTime now = DateTime.Now;
            DateTime epoch = new DateTime(1970, 1, 1);
            return BitConverter.GetBytes((int)(now - epoch).TotalSeconds);
        }

        public PacketCollection QueryServer(string host, int port)
        {
            PacketCollection packets = new PacketCollection();
            //6500
            using (UdpClient client = new UdpClient(56800))
            {
                byte[] request, 
                        response,
                        timestamp = GetTimeStamp();

                IPEndPoint remoteIpEndpoint = new IPEndPoint(IPAddress.Parse(host), port);
                client.Client.ReceiveTimeout = 100;
                client.Connect(remoteIpEndpoint);
                request = GetChallengeRequest(timestamp);
                client.Send(request, request.Length);
                response = client.Receive(ref remoteIpEndpoint);
                ChallengePacket pChallenge = new ChallengePacket(response);

                request = GetInfosRequest(timestamp, pChallenge.GetChallenge());
                client.Send(request, request.Length);

                while (true)
                {
                    try
                    {
                        response = client.Receive(ref remoteIpEndpoint);
                        packets.Add(new InfoPacket(response));
                    }
                    catch (SocketException ex)
                    {
                        if (ex.ErrorCode == (int)SocketError.TimedOut)
                            break;
                        else
                            throw;
                    }
                }
            }

            return packets;
        }

        private byte[] GetChallengeRequest(byte[] timestamp)
        {
            return GetRequestHeader((byte)0x09, timestamp);
        }
        private byte[] GetInfosRequest(byte[] timestamp, byte[] challenge)
        {
            List<byte> result = new List<byte>();
            result.AddRange(GetRequestHeader((byte)0x00, timestamp));
            result.AddRange(challenge);
            result.AddRange(new byte[] { 0xff, 0xff, 0xff, 0x01 });
            return result.ToArray();
        }
        private byte[] GetRequestHeader(byte requestType, byte[] timestamp)
        {
            List<byte> result = new List<byte>();
            result.AddRange(new byte[] { 0xfe, 0xfd });
            result.Add(requestType);
            result.AddRange(timestamp);
            return result.ToArray();
        }
    }

    public class PacketCollection : List<Packet>
    {
        public PacketCollection()
            : base()
        { }
        public PacketCollection(int capacity)
            : base(capacity)
        { }
        public PacketCollection(IEnumerable<Packet> collection)
            : base(collection)
        { }
        public byte[] GetData()
        {
            List<byte> result = new List<byte>();
            foreach (var packet in this)
            {
                result.AddRange(packet.GetData());
            }
            return result.ToArray();
        }
        public string GetDataString()
        {
            return CleanDataString(Encoding.ASCII.GetString(this.GetData()));
        }
        protected string CleanDataString(string rawDataString)
        {
            if (String.IsNullOrWhiteSpace(rawDataString))
                return rawDataString;
            string rs = Encoding.ASCII.GetString(new byte[] { 0x70, 0x6C, 0x61, 0x79, 0x65, 0x72, 0x5F, 0x00, 0x1E });
            string result = rawDataString.Replace(rs, "");
            return result;
        }
    }

    public class Packet
    {
        protected byte[] _buffer;
        private byte _packetType;
        public Packet(byte[] buffer) : this(0, buffer) { }
        public Packet(byte packetType, byte[] buffer)
        {
            this._packetType = packetType;
            this._buffer = buffer;
        }
        public byte[] GetBytes()
        {
            return this._buffer;
        }
        public string GetDataString()
        {
            byte[] data = this.GetData();
            if (data == null)
                return null;
            return Encoding.ASCII.GetString(this.GetData());
        }
        public virtual byte[] GetData()
        {
            return this._buffer;
        }
        protected byte[] GetSelectedBytes(byte[] source, int sourceIndex = 0)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            return this.GetSelectedBytes(source, sourceIndex, source.Length);
        }
        protected byte[] GetSelectedBytes(byte[] source, int sourceIndex, int length)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            byte[] result = new byte[length];
            Array.Copy(source, sourceIndex, result, 0, length);
            return result;
        }
    }

    public class ChallengePacket : ServerPacket
    {
        public ChallengePacket(byte[] buffer)
            : base((byte)0x09, buffer)
        { }
        public byte[] GetChallenge()
        {
            int challenge = Int32.Parse(GetDataString());
            return new byte[] {
                (byte)(challenge >> 24),
                (byte)(challenge >> 16),
                (byte)(challenge >> 8),
                (byte)(challenge >> 0)
            };
        }
    }

    public class InfoPacket : ServerPacket
    {
        public InfoPacket(byte[] buffer)
            : base((byte)0x00, buffer)
        { }
        public override byte[] GetData()
        {
            if (this._buffer == null)
                return null;
            return this.GetSelectedBytes(this._buffer, 16, this._buffer.Length - 16);
        }
    }

    public class ServerPacket : Packet
    {
        public ServerPacket(byte packetType, byte[] buffer)
            : base(packetType, buffer)
        { }
        public override byte[] GetData()
        {
            if (this._buffer == null)
                return null;
            return this.GetSelectedBytes(this._buffer, 5, this._buffer.Length - 5);
        }
    }

    public class Player
    {
        public string Name { get; set; }
        public Player()
            : this(null)
        { }
        public Player(string name)
        {
            this.Name = name;
        }
    }

    public class PlayerCollection : List<Player>
    {
        public PlayerCollection()
            : base()
        { }
        public PlayerCollection(IEnumerable<Player> collection)
            : base(collection)
        { }
        public static PlayerCollection Parse(string data)
        {
            if (String.IsNullOrWhiteSpace(data))
                return new PlayerCollection();

            string[] dataBlocks = Regex.Split(data, Encoding.ASCII.GetString(new byte[] { 00, 00 }), RegexOptions.Compiled);
            if (dataBlocks == null || dataBlocks.Length == 0)
                return new PlayerCollection();

            var players = dataBlocks[1].Split('\0').Distinct();
            if (players == null || players.Count() == 0)
                return new PlayerCollection();

            PlayerCollection collection = new PlayerCollection(
                from s in players
                select new Player(s)
            );
            return collection;
        }
    }

    public class ServerInfo
    {
        public string GameVersion { get; set; }
        public string HostName { get; set; }
        public string MapName { get; set; }
        public string GameType { get; set; }
        public int NumPlayers { get; set; }
        public int NumTeam { get; set; }
        public int MaxPlayers { get; set; }
        public string GameMode { get; set; }
        public string TimeLimit { get; set; }
        public bool Password { get; set; }
        public string CurrentVersion { get; set; }
        public string RequiredVersion { get; set; }
        public string Mod { get; set; }
        public bool BattleEye { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public List<string> Players { get; set; }
        public string Mission { get; set; }

        public static ServerInfo Parse(string data)
        {
            ServerInfo info = new ServerInfo();
            string[] parts = data.Split('\0');
            Dictionary<string, string> values = new Dictionary<string, string>();
            for (int i = 0; i < parts.Length; i++)
            {
                if ((i & 1) == 0 && !values.ContainsKey(parts[i]) && (i + 1) < parts.Length)
                    values.Add(parts[i], parts[i + 1]);
            }

            info.GameVersion = GetValueByKey("gamever", values);
            info.HostName = GetValueByKey("hostname", values);
            info.MapName = GetValueByKey("mapname", values);
            info.GameType = GetValueByKey("gametype", values);
            info.NumPlayers = ParseInt(GetValueByKey("numplayers", values));
            info.NumTeam = ParseInt(GetValueByKey("numteams", values));
            info.MaxPlayers = ParseInt(GetValueByKey("maxplayers", values));
            info.GameMode = GetValueByKey("gamemode", values);
            info.TimeLimit = GetValueByKey("timelimit", values);
            info.Password = ParseBoolean(GetValueByKey("password", values));
            info.CurrentVersion = GetValueByKey("currentVersion", values);
            info.RequiredVersion = GetValueByKey("requiredVersion", values);
            info.Mod = GetValueByKey("mod", values);
            info.BattleEye = ParseBoolean(GetValueByKey("sv_battleye", values));
            info.Longitude = ParseDouble(GetValueByKey("lng", values));
            info.Latitude = ParseDouble(GetValueByKey("lat", values));
            info.Mission = GetValueByKey("mission", values);

            return info;
        }
        private static int ParseInt(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return 0;
            int parsedValue = 0;
            Int32.TryParse(value, out parsedValue);
            return parsedValue;
        }
        private static double ParseDouble(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return 0;
            double parsedValue = 0;
            Double.TryParse(value, out parsedValue);
            return parsedValue;
        }
        private static bool ParseBoolean(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return false;
            if (value == "1" || value.ToLowerInvariant() == "true")
                return true;
            return false;
        }
        private static string GetValueByKey(string key, Dictionary<string, string> values)
        {
            if (values.ContainsKey(key))
                return values[key];
            return null;
        }
    }
}
