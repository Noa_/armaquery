﻿using System;
using System.Text;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ArmA3
{
    class ArmA3
    {
        public ServerInfo ServerInfo { get; protected set; }
        public PlayerCollection Players { get; protected set; }
        public string host;
        public int port;

        public ArmA3(string host, int port)
        {
                this.host = host;
                this.port = port;
        }

        public void Update()
        {
            try
            {
                using (UdpClient client = new UdpClient(56800))
                {
                    byte[] request, response;

                    IPEndPoint remoteIpEndpoint = new IPEndPoint(IPAddress.Parse(host), port);
                    client.Client.ReceiveTimeout = 100;
                    client.Connect(remoteIpEndpoint);

                    //Server Info
                    request = new byte[] { 0xfe, 0xfd, 0x00, 0x43, 0x4f, 0x52, 0x59, 0xff, 0x00, 0x00 };
                    client.Send(request, request.Length);
                    response = client.Receive(ref remoteIpEndpoint);
                    string dataServer = Encoding.ASCII.GetString(response).Remove(0, 5);
                    this.ServerInfo = ServerInfo.Parse(dataServer);

                    //Player Info
                    request = new byte[] { 0xfe, 0xfd, 0x00, 0x43, 0x4f, 0x52, 0x59, 0x00, 0xff, 0xff };
                    client.Send(request, request.Length);
                    response = client.Receive(ref remoteIpEndpoint);
                    string dataPlayer = Encoding.ASCII.GetString(response).Remove(0, 5);
                    this.Players = PlayerCollection.Parse(dataPlayer.Remove(0, 32));
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Sock Error - " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Error - " + ex.Message);
            }
        }
    }

    public class ServerInfo
    {
        public string GameVersion { get; set; }
        public string HostName { get; set; }
        public string MapName { get; set; }
        public string GameType { get; set; }
        public int NumPlayers { get; set; }
        public int NumTeam { get; set; }
        public int MaxPlayers { get; set; }
        public string GameMode { get; set; }
        public string TimeLimit { get; set; }
        public bool Password { get; set; }
        public string CurrentVersion { get; set; }
        public string RequiredVersion { get; set; }
        public string Mod { get; set; }
        public bool BattleEye { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public List<string> Players { get; set; }
        public string Mission { get; set; }

        public static ServerInfo Parse(string data)
        {
            ServerInfo info = new ServerInfo();
            string[] parts = data.Split('\0');
            Dictionary<string, string> values = new Dictionary<string, string>();
            for (int i = 0; i < parts.Length; i++)
            {
                if ((i & 1) == 0 && !values.ContainsKey(parts[i]) && (i + 1) < parts.Length)
                    values.Add(parts[i], parts[i + 1]);
            }

            info.GameVersion = GetValueByKey("gamever", values);
            info.HostName = GetValueByKey("hostname", values);
            info.MapName = GetValueByKey("mapname", values);
            info.GameType = GetValueByKey("gametype", values);
            info.NumPlayers = ParseInt(GetValueByKey("numplayers", values));
            info.NumTeam = ParseInt(GetValueByKey("numteams", values));
            info.MaxPlayers = ParseInt(GetValueByKey("maxplayers", values));
            info.GameMode = GetValueByKey("gamemode", values);
            info.TimeLimit = GetValueByKey("timelimit", values);
            info.Password = ParseBoolean(GetValueByKey("password", values));
            info.CurrentVersion = GetValueByKey("currentVersion", values);
            info.RequiredVersion = GetValueByKey("requiredVersion", values);
            info.Mod = GetValueByKey("mod", values);
            info.BattleEye = ParseBoolean(GetValueByKey("sv_battleye", values));
            info.Longitude = ParseDouble(GetValueByKey("lng", values));
            info.Latitude = ParseDouble(GetValueByKey("lat", values));
            info.Mission = GetValueByKey("mission", values);

            return info;
        }
        private static int ParseInt(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return 0;
            int parsedValue = 0;
            Int32.TryParse(value, out parsedValue);
            return parsedValue;
        }
        private static double ParseDouble(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return 0;
            double parsedValue = 0;
            Double.TryParse(value, out parsedValue);
            return parsedValue;
        }
        private static bool ParseBoolean(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return false;
            if (value == "1" || value.ToLowerInvariant() == "true")
                return true;
            return false;
        }
        private static string GetValueByKey(string key, Dictionary<string, string> values)
        {
            if (values.ContainsKey(key))
                return values[key];
            return null;
        }
    }

        public class Player
    {
        public string Name { get; set; }
        public Player()
            : this(null)
        { }
        public Player(string name)
        {
            this.Name = name;
        }
    }

    public class PlayerCollection : List<Player>
    {
        public PlayerCollection()
            : base()
        { }
        public PlayerCollection(IEnumerable<Player> collection)
            : base(collection)
        { }
        public static PlayerCollection Parse(string data)
        {
            string final = "";
            var players = data.Split('\0');
            int lel = 0;
            foreach (var p in players)
            {
                if (lel == 0)
                {
                    if (p != "")
                    {
                        final += p + "\0";
                    }
                }
                lel++;
                if (lel == 4)
                    lel = 0;
            }
            players = final.Split('\0');

            PlayerCollection collection = new PlayerCollection(
                from s in players
                select new Player(s)
            );
            return collection;
        }
    }
}
